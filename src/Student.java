public class Student extends Person {

    //    ATTRIBUTES
    private int registration;
    private String course;

    //    METHODS
    //    Accessors and Modifiers Methods (Getters and Setters)

    public int getRegistration() {
        return registration;
    }

    public void setRegistration(int registration) {
        this.registration = registration;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    //    Others Methods
    public void monthlyPayment() {

    }

}
