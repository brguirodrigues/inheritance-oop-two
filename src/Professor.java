public class Professor extends Person {

    //    ATTRIBUTES
    private String specialty;
    private double salary;

//    METHODS
//    Accessors and Modifiers Methods (Getters and Setters)

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    //    Others Methods
    public void receiveIncrease(double increase) {
        setSalary(getSalary() + increase);
    }

}
