public abstract class Person {

    //    ATTRIBUTES
    private String name;
    private int age;
    private char sex;

    //    METHODS
    //    Accessors and Modifiers Methods (Getters and Setters)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    //    Others Methods
    public void doBirthday() {
        setAge(getAge() + 1);
        System.out.println("Happy birthday " + this.getName() + "!!");
        System.out.println("Now you have " + this.getAge() + "!!");
    }

}
