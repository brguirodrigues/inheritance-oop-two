public class Scholarship extends Student {

//    ATTRIBUTES
    private String scholarship;

//    METHODS
//    Accessors and Modifiers Methods (Getters and Setters)

    public String getScholarship() {
        return scholarship;
    }

    public void setScholarship(String scholarship) {
        this.scholarship = scholarship;
    }

//    Others Methods
    public void renew(){

    }

    public void monthlyPayment(){

    }

}
