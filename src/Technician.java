public class Technician extends Student{

//    ATTRIBUTES
    private String professionalRegister;

//    METHODS
//    Accessors and Modifiers Methods (Getters and Setters)
    public String getProfessionalRegister() {
        return professionalRegister;
    }

    public void setProfessionalRegister(String professionalRegister) {
        this.professionalRegister = professionalRegister;
    }

//    Others Methods
    public void practice(){

    }


}
